# Amharic translation for camera-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 10:34+0000\n"
"PO-Revision-Date: 2020-08-30 00:27+0000\n"
"Last-Translator: Samson <sambelet@yahoo.com>\n"
"Language-Team: Amharic <https://translate.ubports.com/projects/ubports/"
"camera-app/am/>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: lomiri-camera-app.desktop.in:4 lomiri-camera-app.desktop.in:6
#: Information.qml:106
msgid "Camera"
msgstr "ካሜራ"

#: lomiri-camera-app.desktop.in:5
msgid "Camera application"
msgstr "የ ካሜራ መተግበሪያ"

#: lomiri-camera-app.desktop.in:7
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "ፎቶዎች: ቪዲዮዎች: መያዣ: ማንሻ: ፎቶ ማንሻ: መቅረጫ:"

#: lomiri-camera-app.desktop.in:10
msgid "lomiri-camera-app"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:4
#: lomiri-barcode-reader-app.desktop.in:6
msgid "Barcode Reader"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:5
#, fuzzy
#| msgid "Camera application"
msgid "Barcode Reader application"
msgstr "የ ካሜራ መተግበሪያ"

#: lomiri-barcode-reader-app.desktop.in:7
msgid "QR;Code;Reader"
msgstr ""

#: lomiri-barcode-reader-app.desktop.in:10
msgid "lomiri-barcode-reader-app"
msgstr ""

#: AdvancedOptions.qml:23 PhotogridView.qml:69 SlideshowView.qml:77
msgid "Settings"
msgstr "ማሰናጃ"

#: AdvancedOptions.qml:27
msgid "Close"
msgstr ""

#: AdvancedOptions.qml:35 Information.qml:20 PhotogridView.qml:78
#: SlideshowView.qml:86
msgid "About"
msgstr "ስለ"

#: AdvancedOptions.qml:66
msgid "Add date stamp on captured images"
msgstr "በ ተያዘው ምስል ላይ የ ቀን ማህተም ያድርጉ"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:89
msgid "Format"
msgstr "አቀራረብ"

#: AdvancedOptions.qml:142
msgid "Date formatting keywords"
msgstr "የ ቀን አቀራረብ ቁልፍ ቃል"

#: AdvancedOptions.qml:147
msgid "the day as number without a leading zero (1 to 31)"
msgstr "የ ቀን ቁጥር ያለ ቀዳሚ ዜሮ ከ (1 እስከ 31)"

#: AdvancedOptions.qml:148
msgid "the day as number with a leading zero (01 to 31)"
msgstr "የ ቀን ቁጥር ከ ቀዳሚ ዜሮ ጋር ከ (1 እስከ 31)"

#: AdvancedOptions.qml:149
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "የ ቀን ስም በ አኅጽሮተ ቃል (ለምሳሌ:  'ሰኞ' እስከ 'እሑ')."

#: AdvancedOptions.qml:150
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "የ ቀን ስም በ እረጅም ቃል (ለምሳሌ:  'ሰኞ' እስከ 'እሑ')."

#: AdvancedOptions.qml:151
msgid "the month as number without a leading zero (1 to 12)"
msgstr "ወር እንደ ቁጥር ያለ ቀዳሚ ዜሮ ከ (1 እስከ 12)"

#: AdvancedOptions.qml:152
msgid "the month as number with a leading zero (01 to 12)"
msgstr "ወር እንደ ቁጥር ከ ቀዳሚ ዜሮ ጋር ከ (01 እስከ 12)"

#: AdvancedOptions.qml:153
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "የ ወር ስም በ አኅጽሮተ ቃል (ለምሳሌ:  'መስ' እስከ 'ጳጉሜ')."

#: AdvancedOptions.qml:154
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr "ሙሉ የ ወር ስም  (ለምሳሌ:  'መስከረም' እስከ 'ጳጉሜ')."

#: AdvancedOptions.qml:155
msgid "the year as two digit number (00 to 99)"
msgstr "አመት እንደ ሁለት አሀዝ ቁጥር (00 እስከ 99)"

#: AdvancedOptions.qml:156
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr "አመት እንደ አራት አሀዝ ቁጥር: አመት አሉታዊ ከሆነ: የ መቀነስ ምልክት ይጨመርበታል:"

#: AdvancedOptions.qml:157
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr "ሰአት ያለ ቀዳሚ ዜሮ ከ (0 እስከ 23 ወይንም ከ 1 እስከ 12 ጠዋት/ከሰአት በሚታይ ጊዜ)"

#: AdvancedOptions.qml:158
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr "ሰአት ከ ቀዳሚ ዜሮ ጋር ከ (00 እስከ 23 ወይንም ከ 01 እስከ 12 ጠዋት/ከሰአት በሚታይ ጊዜ)"

#: AdvancedOptions.qml:159
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "ሰአት ያለ ቀዳሚ ዜሮ ከ (0 እስከ 23 ወይንም ከ 0 እስከ 12 ጠዋት/ከሰአት በሚታይ ጊዜ)"

#: AdvancedOptions.qml:160
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "ሰአት ከ ቀዳሚ ዜሮ ጋር ከ (00 እስከ 23: ጠዋት/ከሰአት በሚታይ ጊዜ)"

#: AdvancedOptions.qml:161
msgid "the minute without a leading zero (0 to 59)"
msgstr "ደቂቃ ያለ ቀዳሚ ዜሮ ከ (0 እስከ 59)"

#: AdvancedOptions.qml:162
msgid "the minute with a leading zero (00 to 59)"
msgstr "ደቂቃ ከ ቀዳሚ ዜሮ ጋር ከ (00 እስከ 59)"

#: AdvancedOptions.qml:163
msgid "the second without a leading zero (0 to 59)"
msgstr "ሰከንድ ያለ ቀዳሚ ዜሮ ከ (0 እስከ 59)"

#: AdvancedOptions.qml:164
msgid "the second with a leading zero (00 to 59)"
msgstr "ሰከንድ ከ ቀዳሚ ዜሮ ጋር ከ (0 እስከ 59)"

#: AdvancedOptions.qml:165
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "ሚሊ ሰከንድ ያለ ቀዳሚ ዜሮ ጋር ከ (0 እስከ 999)"

#: AdvancedOptions.qml:166
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "ሚሊ ሰከንድ ከ ቀዳሚ ዜሮ ጋር ከ (0 እስከ 999)"

#: AdvancedOptions.qml:167
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "ይጠቀሙ ጠዋት/ከሰአት ማሳያ: AP ይቀየራል በ አንዱ 'ጠዋት' ወይንም 'ከሰአት'."

#: AdvancedOptions.qml:168
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "ይጠቀሙ ጠዋት/ከሰአት ማሳያ: AP ይቀየራል በ አንዱ 'ጠዋት' ወይንም 'ከሰአት'."

#: AdvancedOptions.qml:169
msgid "the timezone (for example 'CEST')"
msgstr "የ ሰአት ክልል (ለምሳሌ 'CEST')"

#: AdvancedOptions.qml:178
msgid "Add to Format"
msgstr "ወደ አቀራረብ መጨመሪያ"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: AdvancedOptions.qml:209
msgid "Color"
msgstr "ቀለም"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: AdvancedOptions.qml:278
msgid "Alignment"
msgstr "ማሰለፊያ"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: AdvancedOptions.qml:323
msgid "Opacity"
msgstr "በ ውስጡ የማያሳልፍ"

#: AdvancedOptions.qml:346
msgid "Blurred Overlay"
msgstr ""

#: AdvancedOptions.qml:367
msgid "Only Blur Preview overlay"
msgstr ""

#: DeleteDialog.qml:24
msgid "Delete media?"
msgstr "መገናኛውን ላጥፋው?"

#: DeleteDialog.qml:34 PhotogridView.qml:56 SlideshowView.qml:69
msgid "Delete"
msgstr "ማጥፊያ"

#: DeleteDialog.qml:40 ViewFinderOverlay.qml:1203 ViewFinderOverlay.qml:1217
#: ViewFinderOverlay.qml:1265 ViewFinderView.qml:409
msgid "Cancel"
msgstr "መሰረዣ"

#: GalleryView.qml:270
msgid "No media available."
msgstr "ምንም መገናኛ አልተገኘም"

#: GalleryView.qml:305
msgid "Scanning for content..."
msgstr "ይዞታዎችን በማሰስ ላይ..."

#: GalleryViewHeader.qml:84 SlideshowView.qml:46
msgid "Select"
msgstr "ይምረጡ"

#: GalleryViewHeader.qml:85
msgid "Edit Photo"
msgstr "ፎቶ ማረሚያ"

#: GalleryViewHeader.qml:85
msgid "Photo Roll"
msgstr "የ ፎቶ ጥቅል"

#: Information.qml:25
msgid "Back"
msgstr ""

#: Information.qml:76
msgid "Get the source"
msgstr "ምንጩን ያግኙ"

#: Information.qml:77
msgid "Report issues"
msgstr "ችግሩን ያሳውቁን"

#: Information.qml:78
msgid "Help translate"
msgstr "በ መተርጎም ይርዱ"

#: MediaInfoPopover.qml:14
#, qt-format
msgid "Width : %1"
msgstr "ስፋት : %1"

#: MediaInfoPopover.qml:15
#, qt-format
msgid "Height : %1"
msgstr "እርዝመት : %1"

#: MediaInfoPopover.qml:16
#, fuzzy, qt-format
#| msgid "Name : %1"
msgid "Date : %1"
msgstr "ስም : %1"

#: MediaInfoPopover.qml:17
#, fuzzy, qt-format
#| msgid "Name : %1"
msgid "Camera Model : %1"
msgstr "ስም : %1"

#: MediaInfoPopover.qml:18
#, fuzzy, qt-format
#| msgid "Height : %1"
msgid "Copyright : %1"
msgstr "እርዝመት : %1"

#: MediaInfoPopover.qml:19
#, qt-format
msgid "Exposure Time : %1"
msgstr ""

#: MediaInfoPopover.qml:20
#, fuzzy, qt-format
#| msgid "Name : %1"
msgid "F. Number : %1"
msgstr "ስም : %1"

#: MediaInfoPopover.qml:21
#, qt-format
msgid "Sub-File type : %1"
msgstr ""

#: MediaInfoPopover.qml:43
msgid "Media Information"
msgstr "የ መገናኛ መረጃ"

#: MediaInfoPopover.qml:48
#, qt-format
msgid "Name : %1"
msgstr "ስም : %1"

#: MediaInfoPopover.qml:51
#, qt-format
msgid "Type : %1"
msgstr "አይነት : %1"

#: MediaInfoPopover.qml:83
#, fuzzy, qt-format
#| msgid "Width : %1"
msgid "With Flash : %1"
msgstr "ስፋት : %1"

#: MediaInfoPopover.qml:83
msgid "Yes"
msgstr ""

#: MediaInfoPopover.qml:83
msgid "No"
msgstr ""

#: NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "በ አካሉ ላይ ምንም የቀረ ቦታ የለም: ለ መቀጠል ትንሽ ቦታ ነፃ ያድርጉ:"

#: PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "ወደ ግራ ይጥረጉ ፎቶ ለመመልከት"

#: PhotogridView.qml:42 SlideshowView.qml:54
msgid "Share"
msgstr "ማካፈያ"

#: SlideshowView.qml:62
msgid "Image Info"
msgstr "የ ምስል መረጃ"

#: SlideshowView.qml:98
msgid "Edit"
msgstr "ማረሚያ"

#: SlideshowView.qml:477
msgid "Back to Photo roll"
msgstr "ወደ ፎቶ ጥቅል መመለሻ"

#: UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "ማካፈል አልተቻለም"

#: UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "ፎቶ እና ቪዲዮ በ እንድ ጊዜ ማካፈል አልተቻለም"

#: UnableShareDialog.qml:30
msgid "Ok"
msgstr "እሺ"

#: ViewFinderOverlay.qml:469 ViewFinderOverlay.qml:492
#: ViewFinderOverlay.qml:520 ViewFinderOverlay.qml:543
#: ViewFinderOverlay.qml:628 ViewFinderOverlay.qml:695
msgid "On"
msgstr "ማብሪያ"

#: ViewFinderOverlay.qml:474 ViewFinderOverlay.qml:502
#: ViewFinderOverlay.qml:525 ViewFinderOverlay.qml:548
#: ViewFinderOverlay.qml:567 ViewFinderOverlay.qml:633
#: ViewFinderOverlay.qml:705
msgid "Off"
msgstr "ማጥፊያ"

#: ViewFinderOverlay.qml:497
msgid "Auto"
msgstr "በራሱ"

#: ViewFinderOverlay.qml:534
msgid "HDR"
msgstr "ትእይንት ማግኛ"

#: ViewFinderOverlay.qml:572
msgid "5 seconds"
msgstr "5 ሰከንዶች"

#: ViewFinderOverlay.qml:577
msgid "15 seconds"
msgstr "15 ሰከንዶች"

#: ViewFinderOverlay.qml:595
msgid "Fine Quality"
msgstr "ጥሩ ጥራት"

#: ViewFinderOverlay.qml:600
msgid "High Quality"
msgstr "ከፍተኛ ጥራት"

#: ViewFinderOverlay.qml:605
msgid "Normal Quality"
msgstr "መደበኛ ጥራት"

#: ViewFinderOverlay.qml:610
msgid "Basic Quality"
msgstr "መሰረታዊ ጥራት"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ViewFinderOverlay.qml:643
msgid "SD"
msgstr "ማጠራቀሚያ ካርድ"

#: ViewFinderOverlay.qml:651
msgid "Save to SD Card"
msgstr "ማጠራቀሚያ ካርድ ውስጥ ማስቀመጫ"

#: ViewFinderOverlay.qml:656
msgid "Save internally"
msgstr "በ አካሉ ውስጥ ማስቀመጫ"

#: ViewFinderOverlay.qml:700
msgid "Vibrate"
msgstr "ማንቀጥቀጫ"

#: ViewFinderOverlay.qml:1200
msgid "Low storage space"
msgstr "አነስተኛ የማጠራቀሚያ ቦታ"

#: ViewFinderOverlay.qml:1201
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr "እርስዎ ማጠራቀሚያ ላይ ባዶ ቦታ የለም: ሳይቋረጥ ለ መቀጠል: ትንሽ ቦታ ነፃ ያድርጉ:"

#: ViewFinderOverlay.qml:1214
msgid "External storage not writeable"
msgstr "የ ውስጥ ማጠራቀሚያ ላይ መጻፍ አይቻልም"

#: ViewFinderOverlay.qml:1215
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"በ እርስዎ የ ውጪ ማጠራቀሚያ መገናኛ ላይ መጻፍ አልተቻለም:  እንደገና ያውጡት እና ያስገቡት: ይህ ችግሩን ካልፈታው: "
"እርስዎ አካሉን እንደገና ማዘጋጀት አለብዎት:"

#: ViewFinderOverlay.qml:1253
msgid "Cannot access camera"
msgstr "ካሜራው ጋር መድረስ አልተቻለም"

#: ViewFinderOverlay.qml:1254
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"የ ካሜራው መተግበሪያ በቂ ፍቃድ የለውም ካሜራው ጋር ለ መድረስ: ወይንም ሌላ ችግር ተፈጥሯል:\n"
"\n"
"ችግሩን ፍቃድ መስጠት ካልፈታው: እባክዎን ስልኩን አጥፍተው እንደገና ያብሩ:"

#: ViewFinderOverlay.qml:1256
msgid "Edit Permissions"
msgstr "ፍቃዶች ማረሚያ"

#: ViewFinderView.qml:400
msgid "Capture failed"
msgstr "መያዝ አልተቻለም"

#: ViewFinderView.qml:405
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"የ እርስዎን የ ውጪ መገናኛ መቀየር: ማሰናዳት ወይንም አካሉን እንደገና ማስነሳት ምናልባት ችግሩን ሊፈታው ይችላል:"

#: ViewFinderView.qml:406
msgid "Restarting your device might fix the problem."
msgstr "የ እርስዎን አካል እንደገና ማስነሳት ይህን ችግር ሊፈታው ይችል ይሆናል"

#: camera-app.qml:61
msgid "Flash"
msgstr "ፍላሽ"

#: camera-app.qml:62
msgid "Light;Dark"
msgstr "ብርሀን፡ጨለማ"

#: camera-app.qml:65
msgid "Flip Camera"
msgstr "ካሜራ መገልበጫ"

#: camera-app.qml:66
msgid "Front Facing;Back Facing"
msgstr "ከ ፊት ለፊት: ከ ጀርባ"

#: camera-app.qml:69
msgid "Shutter"
msgstr "ሸተር"

#: camera-app.qml:70
msgid "Take a Photo;Snap;Record"
msgstr "ፎቶ ማንሻ:ፎቶ ማንሻ:መቅረጫ"

#: camera-app.qml:73
msgid "Mode"
msgstr "ዘዴ"

#: camera-app.qml:74
msgid "Stills;Video"
msgstr "ድምፅ የሌለው: ቪዲዮ"

#: camera-app.qml:78
msgid "White Balance"
msgstr "ነጭ ማስተካከያ"

#: camera-app.qml:79
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "የብርሀን ሁኔታ: ቀን: ደመናማ: ቤት ውስጥ"

#: camera-app.qml:386
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> ዛሬ የተነሱ ፎቶዎች"

#: camera-app.qml:387
msgid "No photos taken today"
msgstr "ዛሬ ፎቶ አላነሱም"

#: camera-app.qml:397
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> ዘሬ የተቀረጸ ቪዲዮ"

#: camera-app.qml:398
msgid "No videos recorded today"
msgstr "ዘሬ የተቀረጸ ቪዲዮ የለም"

#~ msgid "Gallery"
#~ msgstr "አዳራሽ"

#~ msgid "Version %1"
#~ msgstr "እትም  %1"

#~ msgid "Advanced Options"
#~ msgstr "የ ረቀቀ ምርጫ"
